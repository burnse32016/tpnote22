import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class Q4 {

    @Mock
    FooUtil fu;


    @Test
    void TestQ4() throws NotTheGoodAnsWerToLifeTheUniverseAndEverythingException {
        when(fu.calculus()).thenReturn(10,42);
        Foo f = new Foo("",fu);
        assertEquals(10,f.methodeATester2());
        NotTheGoodAnsWerToLifeTheUniverseAndEverythingException e = assertThrows(NotTheGoodAnsWerToLifeTheUniverseAndEverythingException.class,() ->fu.calculus());
        System.out.println(e.getMessage());
        assertEquals("not yet implemented",e.getMessage());
    }

}
