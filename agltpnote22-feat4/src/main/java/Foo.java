public class Foo {
    private String id;
    private FooUtil fu;


    public Foo(String id) {
        this.id = id;
        fu=new FooUtil();
    }

    public Foo(String id, FooUtil util) {
        this.id = id;
        fu=util;
    }

    public int methodeATester2() throws NotTheGoodAnsWerToLifeTheUniverseAndEverythingException {
        int cal=fu.calculus();
        if (cal!=42){
            throw new NotTheGoodAnsWerToLifeTheUniverseAndEverythingException();
        }
        return cal;
    }

}
